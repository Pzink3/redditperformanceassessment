package zink.android.redditperformaceassessment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Scanner;


public class ToDoItem {
    public String title;
    public String dateadd;
    public String datedue;
    public String category;

    public ToDoItem(String title, String dateadd, String datedue, String category) {
        this.title = title;
        this.dateadd = dateadd;
        this.datedue = datedue;
        this.category = category;
    }
}