package zink.android.redditperformaceassessment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ToDoFragment extends Fragment {
    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private ToDoActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.todolist, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoItem i1 = new ToDoItem("Hi!", "I", "am", "first");
        ToDoItem i2 = new ToDoItem("App Academy", "11/5/15", "Reddit Performance Assessment", "Task2");
        ToDoItem i3 = new ToDoItem("This", "is", "item", "3");
        ToDoItem i4 = new ToDoItem("This", "is", "item", "4");

        activity.ToDoItem.add(i1);
        activity.ToDoItem.add(i2);
        activity.ToDoItem.add(i3);
        activity.ToDoItem.add(i4);

        ToDoAdapter adapter = new ToDoAdapter(activity.ToDoItem);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback) activity;
        this.activity = (ToDoActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }
}

