package zink.android.redditperformaceassessment;

import android.app.FragmentTransaction;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;


import java.util.ArrayList;


public class ToDoActivity extends SingleFragmentActivity implements ActivityCallback{
    public int currentItem;
    ArrayList<ToDoItem> ToDoItem = new ArrayList<>();

    @Override
    protected Fragment createFragment() {
        return new Fragment();
    }
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new ItemFragment();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);

        transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public void onPostSelected(Uri redditPostUri) {

    }
}
