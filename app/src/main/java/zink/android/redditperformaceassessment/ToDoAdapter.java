package zink.android.redditperformaceassessment;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;


public class ToDoAdapter extends RecyclerView.Adapter<ToDoHolder> {
    private ArrayList<zink.android.redditperformaceassessment.ToDoItem> TodoItem = new ArrayList<>();
    private ActivityCallback activityCallback;

    public ToDoAdapter(ArrayList<zink.android.redditperformaceassessment.ToDoItem> TodoItem) {
        this.TodoItem = TodoItem;
    }

    @Override
    public int getItemCount() {
        return TodoItem.size();
    }

    @Override
    public ToDoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater.from(parent.getContext()));
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ToDoHolder(view);
    }

    @Override
    public void onBindViewHolder(ToDoHolder holder, int position) {
        holder.titleText.setText(TodoItem.get(position).title);
    }
}