package zink.android.redditperformaceassessment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;



public class ToDoHolder extends RecyclerView.ViewHolder {
    public TextView titleText;

    public ToDoHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}

